package com.gleydar.disbot.model.consumers;

import com.gleydar.disbot.controller.events.MessageHandler;
import net.dv8tion.jda.core.entities.Message;

/**
 * Abstract class for every MessageConsumer that needs to know about all new Messages in Real-Time
 *
 * @author Gleydar
 */

public abstract class MessageConsumer {

	public MessageConsumer() {
		register();
	}

	public abstract void handle(Message m);

	public final void register() {
		MessageHandler.registerConsumer(this);
	}

}
