package com.gleydar.disbot.model.module;

import com.gleydar.disbot.controller.events.MessageHandler;
import com.gleydar.disbot.model.user.DisUser;
import com.gleydar.disbot.model.permissions.Permission;
import net.dv8tion.jda.core.entities.Message;

/**
 * @author Konstantin on 17.08.2017.
 */
public abstract class Module implements IModule {

	protected DisUser author;

	public Module() {
		register();
	}

	private void register() {
		MessageHandler.registerModule(this);
	}

	private void unregister() {
		MessageHandler.unregisterModule(this);
	}

	public abstract void execute(String[] args, Message m) throws Exception;

	public abstract Permission getPermission();

	public abstract String getLabel();

	public void call(String[] args, Message m) throws Exception {
		//TODO
		this.author = null;
		this.execute(args, m);
	}
}
