package com.gleydar.disbot.model.module;

import com.gleydar.disbot.model.permissions.Permission;
import net.dv8tion.jda.core.entities.Message;

/**
 * IModule Interface Class
 *
 * @author Gleydar
 */

public interface IModule {

	void execute(String[] args, Message m) throws Exception;

	Permission getPermission();

	String getLabel();

	void call(String[] args, Message m) throws Exception;

}
