package com.gleydar.disbot.model.permissions;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.model.user.DisUser;
import net.dv8tion.jda.core.entities.Guild;

import javax.persistence.*;
import java.util.List;

/**
 * Implementation for Groups in the Bot
 * Created by Gleydar on 17.03.2017.
 */
@Entity
public class Group {
	@Id
	@GeneratedValue
	private long id;
	private String guildId;
	@Transient
	private Guild guild;
	@ManyToOne
	private List<DisUser> users;
	@ElementCollection
	private List<Permission> permissions;

	/**
	 * Default-Constructor für JPA
	 */
	public Group() {

	}

	public String getGuildId() {
		return guildId;
	}

	public void setGuildId(String guildId) {
		this.guildId = guildId;
		this.guild = DisBot.getClient().getGuildById(guildId);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Guild getGuild() {
		return guild;
	}

	public void setGuild(Guild guild) {
		this.guild = guild;
	}

	public List<DisUser> getUsers() {
		return users;
	}

	public void setUsers(List<DisUser> users) {
		this.users = users;
	}

	public List<Permission> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<Permission> permissions) {
		this.permissions = permissions;
	}
}
