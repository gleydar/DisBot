package com.gleydar.disbot.model.permissions;

import javax.persistence.Embeddable;
import java.util.Objects;

/**
 * Permission class with basic functionality.
 * Created by Gleydar on 22.02.2017.
 */
@Embeddable
public class Permission {

	private String perm;

	public Permission(String perm) {
		this.perm = perm;
	}

	public String getPerm() {
		return perm;
	}

	public boolean equals(Permission permission) {
		return Objects.equals(permission.getPerm(), this.getPerm());
	}

	public boolean isPositive() {
		return !perm.contains("-");
	}

	public boolean contains(Permission perm) {
		if (getPerm().contains(".") && perm.getPerm().contains(".")) {
			String[] our = getPerm().split(".");
			String[] other = getPerm().split(".");

			for (int i = 0; i < our.length; i++) {
				if (!our[i].equals(other[i]) && !our[i].equals("*")) {
					return false;
				}
				if (our[i].equals("*")) {
					return true;
				}
			}

		} else {
			if (getPerm().equals("*") || getPerm().equals(perm.getPerm())) {
				return true;
			}
		}
		return true;
	}

}
