package com.gleydar.disbot.model.permissions;

import net.dv8tion.jda.core.entities.Guild;

import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import java.util.List;
import java.util.Map;

/**
 * @author Konstantin on 28.06.2018.
 */
@Embeddable
public class UserPermissions {
	@ElementCollection
	private List<Permission> globalPermissions;
	@ElementCollection
	private Map<Guild, List<Permission>> guildPermissions;

	public UserPermissions() {

	}

	public List<Permission> getGlobalPermissions() {
		return globalPermissions;
	}

	public void setGlobalPermissions(List<Permission> globalPermissions) {
		this.globalPermissions = globalPermissions;
	}

	public Map<Guild, List<Permission>> getGuildPermissions() {
		return guildPermissions;
	}

	public void setGuildPermissions(Map<Guild, List<Permission>> guildPermissions) {
		this.guildPermissions = guildPermissions;
	}
}
