package com.gleydar.disbot.model.user;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.model.permissions.UserPermissions;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.PrivateChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.requests.RestAction;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;
import java.util.List;

/**
 * Class that extends the User implementation of JDA with specific functionality.
 *
 * @author Gleydar
 */
@Entity
public class DisUser implements User {

	@Id
	private String id;
	private String userId;
	private List<String> guildIds;
	@Embedded
	private UserPermissions permissions;
	@Transient
	private User user;
	@Transient
	private List<Guild> guilds;

	/**
	 * Default Constructor für JPA
	 */
	public DisUser() {

	}

	public void setId(String id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
		this.user = DisBot.getClient().getUserById(userId);
		this.id = user.getId();
	}

	public List<String> getGuildIds() {
		return guildIds;
	}

	public void setGuildIds(List<String> guildIds) {
		this.guildIds = guildIds;
	}

	public List<Guild> getGuilds() {
		return guilds;
	}

	public void setGuilds(List<Guild> guilds) {
		this.guilds = guilds;
	}

	public UserPermissions getPermissions() {
		return permissions;
	}

	public void setPermissions(UserPermissions permissions) {
		this.permissions = permissions;
	}

	@Override
	public String getName() {
		return user.getName();
	}

	@Override
	public String getDiscriminator() {
		return user.getDiscriminator();
	}

	@Override
	public String getAvatarId() {
		return user.getAvatarId();
	}

	@Override
	public String getAvatarUrl() {
		return user.getAvatarUrl();
	}

	@Override
	public String getDefaultAvatarId() {
		return user.getDefaultAvatarId();
	}

	@Override
	public String getDefaultAvatarUrl() {
		return user.getDefaultAvatarUrl();
	}

	@Override
	public String getEffectiveAvatarUrl() {
		return user.getEffectiveAvatarUrl();
	}

	@Override
	public boolean hasPrivateChannel() {
		return user.hasPrivateChannel();
	}

	@Override
	public RestAction<PrivateChannel> openPrivateChannel() {
		return user.openPrivateChannel();
	}

	@Override
	public List<Guild> getMutualGuilds() {
		return user.getMutualGuilds();
	}

	@Override
	public boolean isBot() {
		return user.isBot();
	}

	@Override
	public JDA getJDA() {
		return user.getJDA();
	}

	@Override
	public boolean isFake() {
		return user.isFake();
	}

	@Override
	public String getAsMention() {
		return user.getAsMention();
	}

	@Override
	public long getIdLong() {
		return user.getIdLong();
	}

	@Override
	public String getId() {
		return user.getId();
	}
}
