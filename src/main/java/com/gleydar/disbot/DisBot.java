package com.gleydar.disbot;

import com.gleydar.disbot.database.IPermissionDatabaseHandler;
import com.gleydar.disbot.database.implementations.MariaDbHandler;
import com.gleydar.disbot.controller.events.MessageHandler;
import com.gleydar.disbot.controller.events.StartEvent;
import com.gleydar.disbot.controller.modules.chat.History;
import com.gleydar.disbot.controller.modules.fun.Imgur;
import com.gleydar.disbot.controller.modules.fun.Reddit;
import com.gleydar.disbot.controller.modules.fun.SelfInfo;
import com.gleydar.disbot.controller.modules.fun.WolframAlpha;
import com.gleydar.disbot.controller.modules.help.Help;
import com.gleydar.disbot.controller.modules.help.Invite;
import com.gleydar.disbot.controller.modules.tools.Info;
import com.gleydar.disbot.controller.modules.tools.Purge;
import com.gleydar.disbot.io.sockets.MinecraftSocket;
import com.gleydar.disbot.utils.ConfigUtil;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import org.apache.commons.logging.impl.SimpleLog;

import java.util.Random;
import java.util.logging.Level;

/**
 * Main class of the Bot.
 *
 * @author Gleydar
 */
public class DisBot {

	private static final SimpleLog l = new SimpleLog("DisBot");
	private final static String TOKEN = ConfigUtil.getStringValue("bot-token");
	public final static String IMGUR_ID = ConfigUtil.getStringValue("imgur-appid");
	public final static Random r = new Random();
	private static final IPermissionDatabaseHandler PERMISSION_HANDLER = new MariaDbHandler(
			ConfigUtil.getStringValue("database-host"),
			ConfigUtil.getStringValue("database-name"),
			ConfigUtil.getStringValue("database-port"),
			ConfigUtil.getStringValue("database-user"),
			ConfigUtil.getStringValue("database-password")
	);
	private static JDA client;


	public static void main(String[] args) {
		try {
			manageStart();
			registerModules();
			MinecraftSocket.run();
		} catch (Exception ex) {
			l.error("Exception:", ex);
		}
	}

	private static void manageStart() {
		JDABuilder b = new JDABuilder(AccountType.BOT).setToken(TOKEN);
		try {
			client = registerEvents(b).buildBlocking();
		} catch (Exception ex) {
			java.util.logging.Logger.getLogger(DisBot.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private static JDABuilder registerEvents(JDABuilder b) {
		b.addEventListener(new MessageHandler());
		b.addEventListener(new StartEvent());
		return b;
	}

	private static void registerModules() {
		new Invite();
		new Help();
		new SelfInfo();
		new Info();
		new WolframAlpha();
		new Imgur();
		new Reddit();
		new Purge();
		new History();
	}

	public static JDA getClient() {
		return client;
	}

	public static IPermissionDatabaseHandler getDatabaseHandler() {
		return PERMISSION_HANDLER;
	}

}
