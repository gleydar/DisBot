package com.gleydar.disbot.controller.events;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.model.module.IModule;
import com.gleydar.disbot.model.user.DisUser;
import com.gleydar.disbot.model.consumers.MessageConsumer;
import com.gleydar.disbot.utils.MessageUtil;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author Gleydar
 */

public class MessageHandler extends ListenerAdapter {

	private static ArrayList<MessageConsumer> consumers = new ArrayList<>();
	private static HashMap<String, IModule> modules = new HashMap<>();
	private static final File LOG = new File("commands.log");

	@Override
	public void onMessageReceived(MessageReceivedEvent e) {
		new Thread(() -> {
			Message m = e.getMessage();
			JDA c = DisBot.getClient();

			for (MessageConsumer mc : consumers) {
				mc.handle(m);
			}

			if (e.isFromType(ChannelType.PRIVATE) || e.isFromType(ChannelType.GROUP) || containsUser(m)) {
				String[] args;
				String l;
				String[] s;
				DisUser user;

				if (e.isFromType(ChannelType.PRIVATE) || e.isFromType(ChannelType.GROUP)) {
					user = DisUser.getUser(new UserEnvironment(e.getAuthor(), null));
					s = m.getContentStripped().split(" ");
				} else {
					user = DisUser.getUser(new UserEnvironment(e.getAuthor(), m.getGuild()));
					s = m.getContentStripped().replaceAll("@\\p{L}+ ", "").split(" ");
				}

				l = s[0];
				args = new String[s.length - 1];

				System.arraycopy(s, 1, args, 0, s.length - 1);

				if (modules.containsKey(l)) {
					PrintWriter out = null;

					try {
						out = new PrintWriter(new BufferedWriter(new FileWriter(LOG, true)));
					} catch (IOException ex) {
						Logger.getLogger(MessageHandler.class.getName()).log(Level.SEVERE, null, ex);
					}

					StringBuilder sb = new StringBuilder().append(LocalDateTime.now()).append(" ").append(m.getAuthor()).append(" used command ").append(l).append(" with Arguments ")
							.append(Arrays.toString(args));

					if (user.hasPermission(modules.get(l).getPermission())) {
						try {
							modules.get(l).call(args, m);
						} catch (Exception ex) {
							MessageUtil.sendChannelMessage(m.getChannel(), "You broke me!! I'll tell my dev about that!");
							sb.append(" and returned an error. ").append(ex.getClass().getName()).append(" Stack Trace:").append(Arrays.toString(ex.getStackTrace()));
							sb.append(" Message: ").append(ex.getMessage());
						}
					} else {
						MessageUtil.sendChannelMessage(m.getChannel(), "Sorry " + m.getAuthor().getAsMention() + ", but you are not allowed to do that :C");
						sb.append(" and was denied because of missing permissions");
					}

					out.println(sb.toString());
					out.flush();
				}
			}
		}).start();
	}


	private boolean containsUser(Message m) {
		return m.getMentionedUsers().contains(DisBot.getClient().getSelfUser()) && !m.getMentionedUsers().isEmpty();
	}

	public static void registerConsumer(MessageConsumer mc) {
		if (!consumers.contains(mc)) {
			consumers.add(mc);
		}
	}

	public static void registerModule(IModule m) {
		String l = m.getLabel();

		if (!modules.containsKey(l)) {
			modules.put(l, m);
		}

	}

	public static void unregisterModule(IModule m) {
		String l = m.getLabel();

		if (modules.containsKey(l)) {
			modules.remove(l);
		}
	}


}
