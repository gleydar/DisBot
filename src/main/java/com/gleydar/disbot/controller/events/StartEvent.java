package com.gleydar.disbot.controller.events;

import com.gleydar.disbot.model.user.DisUser;
import com.gleydar.disbot.utils.MessageUtil;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;


/**
 *
 * @author Gleydar
 */

public class StartEvent extends ListenerAdapter{
    
    public StartEvent(){
        
    }

    @Override
    public void onReady(ReadyEvent e){
        DisUser disUser = DisUser.getUser(new UserEnvironment(e.getJDA().getUserById(143034827019517953L), e.getJDA().getGuildById(149555946895572992L)));
        MessageUtil.sendPrivateMessage(disUser, "I'm back my love <3");
    }
    
}
