package com.gleydar.disbot.controller.modules.tools;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.model.module.Module;
import com.gleydar.disbot.model.permissions.Permission;
import com.gleydar.disbot.utils.MessageUtil;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageHistory;
import net.dv8tion.jda.core.exceptions.PermissionException;

/**
 * @author Gleydar
 * @TODO
 */

public class Purge extends Module {

	@Override
	public void execute(String[] args, Message m) throws Exception {
		if (!m.isFromType(ChannelType.PRIVATE)) {

			switch (args.length) {
				case 0:
					try {
						MessageHistory mh = new MessageHistory(m.getChannel());
						mh.retrievePast(100);
						for (Message me : mh.getRetrievedHistory()) {
							me.delete();
						}
					} catch (Exception e) {
						MessageUtil.sendChannelMessage(m.getChannel(), "Sorry, I can't do that :C");
					}
					break;
				case 1:
					try {
						int i = Integer.parseInt(args[0]);
						MessageHistory mh = new MessageHistory(m.getChannel());
						mh.retrievePast(i + 1);
						for (Message me : mh.getRetrievedHistory()) {
							me.delete();
						}
					} catch (NumberFormatException ex) {
						m.getChannel().sendMessage("Sorry, that is not a valid number.");
					} catch (PermissionException es) {
						int i = Integer.parseInt(args[0]);
						MessageHistory mh = new MessageHistory(m.getChannel());
						mh.retrievePast(100);
						for (Message me : mh.getRetrievedHistory()) {
							if (me.getAuthor().getJDA().equals(DisBot.getClient())) {
								try {
									me.delete();
									i--;
									if (i <= 0) {
										break;
									}
								} catch (Exception e) {

								}
							}
						}
					}
					break;
			}
		} else {
			MessageUtil.sendPrivateMessage(author, "You can't do that in a private channel, silly");
		}
	}

	@Override
	public Permission getPermission() {
		return new Permission("module.tools.purge");
	}

	@Override
	public String getLabel() {
		return "purge";
	}

}
