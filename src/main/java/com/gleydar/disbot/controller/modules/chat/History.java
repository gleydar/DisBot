package com.gleydar.disbot.controller.modules.chat;

import com.gleydar.disbot.model.module.Module;
import com.gleydar.disbot.model.permissions.Permission;
import com.gleydar.disbot.utils.MessageUtil;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageHistory;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * Command that uploads the channels history to ghostbin.
 *
 * @author Gleydar
 */
public class History extends Module {

    @Override
    public void execute(String[] args, Message m) throws Exception {
        if (!m.isFromType(ChannelType.PRIVATE)) {
            StringBuilder text = new StringBuilder();
            boolean normal = true;
            int ammount = 100;
            List<Message> messageList = new ArrayList<>();

            MessageHistory messageHistory = null;
            switch (args.length) {
                case 0:
                    messageHistory = new MessageHistory(m.getChannel());
                    break;
                case 1:
                    try {
                        ammount = Integer.parseInt(args[0]);
                        messageHistory = new MessageHistory(m.getChannel());
                    } catch (NumberFormatException e) {
                        messageHistory = new MessageHistory(m.getMentionedChannels().get(0));
                    } catch (IndexOutOfBoundsException e) {
                        MessageUtil.sendChannelMessage(m.getChannel(), "Sorry, but you have to mention a channel for this to work.");
                    }
                case 2:
                    try {
                        ammount = Integer.parseInt(args[0]);

                        messageHistory = new MessageHistory(m.getMentionedChannels().get(0));
                    } catch (NumberFormatException e) {
                        MessageUtil.sendChannelMessage(m.getChannel(), "Sorry, but that argument has to be a number.");
                    } catch (IndexOutOfBoundsException e) {
                        MessageUtil.sendChannelMessage(m.getChannel(), "Sorry, but you have to mention a channel for this to work.");
                    }

            }

            if (ammount > 100) {
                for (int i = ammount; i >= 100; i -= 100) {
                    messageHistory.retrievePast(i).submit().get();
                    ammount -= 100;

                    if (i - 100 < 100) {
                        messageHistory.retrievePast(i).submit().get();
                    }
                }
            } else {
                messageHistory.retrievePast(100).submit().get();
            }
            messageList.addAll(messageHistory.getRetrievedHistory());

            //Determines the format of the paste, weather it is backwards or forwards
            if (normal) {
                for (int i = messageList.size() - 1; i > 0; i--) {
                    Message message = messageList.get(i);
                    text.append("[").append(message.getCreationTime().getDayOfMonth()).append(".").append(message.getCreationTime().getMonth().ordinal()).append(".").append(message.getCreationTime().getYear()).append(" - ");
                    text.append(message.getCreationTime().toLocalTime()).append("] ").append("<").append(message.getAuthor().getName()).append(" (").append(message.getAuthor().getId()).append(")> ");
                    text.append(message.getContentRaw());
                    text.append("\n");
                }
            } else {
                for (Message message : messageList) {
                    text.append("[").append(message.getCreationTime().getDayOfMonth()).append(".").append(message.getCreationTime().getMonth().ordinal()).append(".").append(message.getCreationTime().getYear()).append(" - ");
                    text.append(message.getCreationTime().toLocalTime()).append("] ").append("<").append(message.getAuthor().getName()).append(" (").append(message.getAuthor().getId()).append(")> ");
                    text.append(message.getContentRaw()).append("\n");
                }
            }


            //Not really random, I know, but it does what it is supposed to. Expires after 1 day anyways.
            String password = RandomStringUtils.randomAlphanumeric(6);

            HttpResponse<String> response = Unirest.post("https://www.ghostbin.com/paste/new")
                    .header("charset", "utf8")
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .field("text", text.toString())
                    .field("password", password)
                    .field("expire", "1d")
                    .field("lang", "irc").asStringAsync().get();
            String name = new StringBuilder(response.getBody()).substring(response.getBody().indexOf("Paste") + 6, response.getBody().indexOf("Paste") + 14);
            MessageUtil.sendChannelMessage(m.getChannel(), "Your Log will be aviable for `1 day` at <https://www.ghostbin.com/paste/" + name + "> with the password `" + password + "`");
        }
    }

    @Override
    public Permission getPermission() {
        return new Permission("module.chat.history");
    }

    @Override
    public String getLabel() {
        return "history";
    }

}
