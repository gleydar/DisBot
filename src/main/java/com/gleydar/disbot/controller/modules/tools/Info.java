package com.gleydar.disbot.controller.modules.tools;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.model.module.Module;
import com.gleydar.disbot.model.permissions.Permission;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.*;

/**
 * @author Gleydar
 * @TODO
 */

public class Info extends Module {

	@Override
	public void execute(String[] args, Message m) throws Exception {
		JDA c = DisBot.getClient();
		StringBuilder r = new StringBuilder();
		switch (args[0].toLowerCase()) {
			case "user":

				if (args.length == 2) {
					User u = null;

					try {
						u = c.getUserById(args[1]);
						u.getDiscriminator();
					} catch (Exception e) {
						if (c.getUsersByName(args[1], true).size() == 1) {
							u = c.getUsersByName(args[1], true).get(0);
						} else {
							m.getChannel().sendMessage("There are multiple users with that name: ");
							for (User us : c.getUsersByName(args[1], true)) {
								m.getChannel().sendMessage("`" + us.getName() + "` with ID `" + us.getId() + "`");
							}

						}
					}

					if (u != null) {
						r.append("User `").append(u.getName()).append("` with ID `").append(u.getId()).append("` ");

						if (!m.isFromType(ChannelType.PRIVATE)) {
							r.append("has roles: ");
							Guild g = m.getGuild();
							m.getChannel().sendMessage(r.toString());
							r.delete(0, r.length());
						} else {
							m.getChannel().sendMessage(r.toString());
							r.delete(0, r.length());
						}
					}

				} else {
					m.getAuthor().openPrivateChannel().complete().sendMessage("Sorry, those are not valid arguments :c");
				}

				break;
			case "channel":
				if (args.length == 1) {
					if (!m.isFromType(ChannelType.PRIVATE)) {
						r.append("`").append(m.getChannel().getName()).append("` with the ID ` ").append(m.getChannel().getId());
						r.append("` has topic `").append(m.getTextChannel().getTopic()).append("`");
						m.getChannel().sendMessage(r.toString());
					} else {
						m.getAuthor().openPrivateChannel().complete().sendMessage("Sorry, that doesnt work with private channels!");
					}
				} else {
					try {
						r.append("Channel ").append(c.getTextChannelById(args[1]).getName()).append(" with ID ").append(args[1]).append(" is on Server ").append(c.getTextChannelById(args[1]).getGuild());
						m.getChannel().sendMessage(r.toString());
					} catch (Exception e) {

						if (c.getTextChannelsByName(args[1], true).size() > 1) {
							String id = c.getTextChannelsByName(args[1], true).get(0).getId();
							r.append("`").append(c.getTextChannelById(id)).append("` with the ID ` ").append(id);
							if (c.getTextChannelsByName(args[1], true).get(0).getTopic().length() > 0) {
								r.append("` has topic `").append(c.getTextChannelsByName(args[1], true).get(0).getTopic()).append("`");
							} else {
								r.append("` has no topic.");
							}
							m.getChannel().sendMessage(r.toString());
						} else {
							for (TextChannel tc : c.getTextChannelsByName(args[1], true)) {
								r.append("`").append(tc).append("` with the ID ` ").append(tc.getId());
								if (tc.getTopic().length() > 0) {
									r.append("` has topic `").append(tc.getTopic()).append("`");
								} else {
									r.append("` has no topic.");
								}
								m.getChannel().sendMessage(r.toString());
								r.delete(0, r.length());
							}
						}
					}
				}
				break;

			case "guild":
				try {
					r.append(m.getGuild().getId());
					r.append(c.getGuildById(args[1]));
					r.append(c.getUserById(c.getGuildById(args[1]).getOwner().getUser().getId()));
					for (Member u : c.getGuildById(args[1]).getMembers()) {
						r.append(u.getUser().getName());
					}
				} finally {
					m.getAuthor().openPrivateChannel().complete().sendMessage(r.toString());
				}
		}
	}

	@Override
	public Permission getPermission() {
		return new Permission("module.tools.info");
	}

	@Override
	public String getLabel() {
		return "info";
	}

}
