package com.gleydar.disbot.controller.modules.help;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.model.module.Module;
import com.gleydar.disbot.model.permissions.Permission;
import com.gleydar.disbot.utils.MessageUtil;
import net.dv8tion.jda.core.entities.Message;


/**
 * @author Gleydar
 */

public class Invite extends Module {

	@Override
	public String getLabel() {
		return "invite";
	}

	@Override
	public void execute(String[] args, Message m) throws Exception {
		MessageUtil.sendChannelMessage(m.getChannel(), "Hello! I'm " + DisBot.getClient().getSelfUser().getName() + "! If you are a server admin and you want to add me to your server, here is your link: \n " +
				m.getJDA().asBot().getInviteUrl(net.dv8tion.jda.core.Permission.MANAGE_PERMISSIONS, net.dv8tion.jda.core.Permission.MESSAGE_MANAGE) +
				" Just give or revoke all the permissions you want me to have :)");
	}

	@Override
	public Permission getPermission() {
		return new Permission("module.help.invite");
	}

}
