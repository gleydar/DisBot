package com.gleydar.disbot.controller.modules.help;

import com.gleydar.disbot.model.module.Module;
import com.gleydar.disbot.model.permissions.Permission;
import net.dv8tion.jda.core.entities.Message;

/**
 * @author Gleydar
 */

public class Commands extends Module {

	@Override
	public void execute(String[] args, Message m) throws Exception {

	}

	@Override
	public Permission getPermission() {
		return new Permission("module.help.commands");
	}

	@Override
	public String getLabel() {
		return "commands";
	}

}
