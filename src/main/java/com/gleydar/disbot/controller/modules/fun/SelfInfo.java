package com.gleydar.disbot.controller.modules.fun;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.model.module.Module;
import com.gleydar.disbot.model.permissions.Permission;
import com.gleydar.disbot.utils.MessageUtil;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Icon;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.managers.AccountManager;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;


/**
 * @author Antimonyy
 */

public class SelfInfo extends Module {

	@Override
	public void execute(String[] args, Message m) throws Exception {

		JDA c = DisBot.getClient();
		AccountManager am = new AccountManager(c.getSelfUser());
		switch (args[0].toLowerCase()) {
			case "avatar":
				URL url = new URL(args[1]);
				BufferedImage img = ImageIO.read(url);
				File avatar = new File("ava.jpg");
				if (avatar.exists()) {
					avatar.delete();
				}
				ImageIO.write(img, "jpg", avatar);
				am.setAvatar(Icon.from(avatar));
				break;
			case "name":
				StringBuilder n = new StringBuilder();
				for (int i = 1; i < args.length; i++) {
					if (i == 1) {
						n = new StringBuilder(args[1]);
					} else {
						n.append(" ").append(args[i]);
					}
				}
				am.setName(n.toString());
				break;
        /*    case "status":
                String s = "";
                    for(int i = 1; i < args.length; i++){
                        s = s + " " + args[i];
                    }
                
                
                break;
             case "idle":
                if(c.getSelfInfo().getOnlineStatus().equals(OnlineStatus.ONLINE)){
                    c.getAccountManager().setIdle(true);
                }else{
                    c.getAccountManager().setIdle(false);
                }
                break; */
			default:
				MessageUtil.sendPrivateMessage(author, "Sorry, unknown arguments :/");
				break;
		}
	}

	@Override
	public Permission getPermission() {
		return new Permission("module.admin.account");
	}

	@Override
	public String getLabel() {
		return "selfinfo";
	}


}
