package com.gleydar.disbot.controller.modules.fun;

import com.gleydar.disbot.model.module.Module;
import com.gleydar.disbot.model.permissions.Permission;
import com.gleydar.disbot.utils.ConfigUtil;
import com.gleydar.disbot.utils.MessageUtil;
import net.dv8tion.jda.core.entities.Message;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;

import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

/**
 * @author Antimonyy
 */

public class WolframAlpha extends Module {

	private final String ID = ConfigUtil.getStringValue("wolframalpha-appid");

	@Override
	public void execute(String[] args, Message m) throws Exception {
		switch (args[0]) {
			default:
				StringBuilder query = new StringBuilder();
				for (String s : args) {
					query.append(s).append(" ");
				}

				String url = "http://api.wolframalpha.com/v2/query?input=" + URLEncoder.encode(query.toString(), "UTF-8") + "&appid=" + ID;
				SAXBuilder builder = new SAXBuilder();
				StringBuilder message = new StringBuilder();
				message.append("For query `").append(query.toString().replace("%20", " ")).append("` WolframAlpha returned as most likely result the type `");

				try {
					Document document = builder.build(new InputSource(new URL(url).openStream()));
					Element rootNode = document.getRootElement();
					List<Element> list = rootNode.getChildren("pod");

					boolean found = false;
					for (Element e : list) {
						switch (e.getAttributeValue("title")) {
							case "Notable Facts":
								message.append("Notable facts` (prepare) ```").append(e.getChild("subpod").getChildText("plaintext")).append("```");
							case "Decimal approximation":
							case "Result":
							case "Basic Definition":
								if (e.getChild("subpod").getChildText("plaintext").length() > 0) {
									message.append(e.getAttributeValue("title")).append("` with the value `").append(e.getChild("subpod").getChildText("plaintext")).append("`");
								} else {
									message.append("Picture` with the URL: ").append(e.getChild("subpod").getChild("img").getAttributeValue("src"));
								}
								found = true;
								break;
							default:

						}
					}
					if (!found) {
						try {
							Element e = list.get(1);
							message.append(e.getAttributeValue("title"));

							if (e.getChild("subpod").getChildText("plaintext").length() > 0) {
								message.append("` with the value ```").append(e.getChild("subpod").getChildText("plaintext")).append("```");
							} else if (e.getChild("subpod").getChild("img").getAttributeValue("src").length() > 0) {
								message.append("` with the URL: ").append(e.getChild("subpod").getChild("img").getAttributeValue("src"));
								try {
									if (!e.getChild("infos").getAttributeValue("count").equals(0)) {
										message.append(" further Infos at ").append(e.getChild("infos").getChild("info").getChild("link").getAttributeValue("url"));
									}
								} catch (Exception ec) {

								}
							} else {
								message.append("` with no further value provided (that I can process)");
							}
						} catch (IndexOutOfBoundsException e) {
							message.append("Nothing`. It literally couldn't handle the request.");
						}
					}
				} catch (IOException io) {
					System.out.println(io.getMessage());
				}

				MessageUtil.sendChannelMessage(m.getChannel(), message.toString());
		}
	}

	@Override
	public Permission getPermission() {
		return new Permission("module.fun.wolfram");
	}

	@Override
	public String getLabel() {
		return ("wa");
	}

}
