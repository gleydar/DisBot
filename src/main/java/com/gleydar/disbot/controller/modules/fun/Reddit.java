package com.gleydar.disbot.controller.modules.fun;

import com.gleydar.disbot.model.module.Module;
import com.gleydar.disbot.model.permissions.Permission;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import net.dv8tion.jda.core.entities.Message;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;


/**
 * @author Antimonyy
 */

public class Reddit extends Module {

    @Override
    public void execute(String[] args, Message m) throws Exception {
        try {
            String subreddit = "random";
            if (args.length >= 1) {
                subreddit = args[0];
            }
            HttpResponse<JsonNode> r = Unirest.get("https://www.reddit.com/r/" + subreddit + "/random.json")
                    .header("User-Agent", "discord:com.gleydar.disbot (by /u/gleydar)").asJson();

            JSONArray a = r.getBody().getArray();
            //Fucking hell, WHY REDDIT WHY
            JSONObject o = a.getJSONObject(0).getJSONObject("data").getJSONArray("children").getJSONObject(0).getJSONObject("data");
            StringBuilder message = new StringBuilder();

            message.append("Here is your random post out of /r/").append(subreddit).append(" with the title: `").append(o.getString("title")).append("`\n");
            String url_i = o.getString("url");
            if (url_i.contains("imgur") && url_i.contains("gifv")) {
                url_i = url_i.replace("gifv", "gif");
            }
            message.append("<").append(url_i).append(">").append("\n");

            if (o.getString("selftext").length() > 1) {
                message.append("```").append(o.getString("selftext").replace("https://www.reddit.com", ""));

                while (message.length() + 3 > 2000) {
                    int b = message.substring(0, 1996).lastIndexOf(" ");
                    m.getChannel().sendMessage(message.substring(0, b) + "```").queue();
                    message.delete(0, b);
                    message.insert(0, "```");
                }

                m.getChannel().sendMessage(message.append("```").toString()).queue();
            } else {
                m.getChannel().sendMessage(message.toString()).queue();
            }

        } catch (Exception e) {
            m.getChannel().sendMessage("Sorry, but I encountered an error while doing that :c Does the specified subreddit exist?").queue();
            System.out.println(e.getMessage());
            System.out.println(Arrays.toString(e.getStackTrace()));
        }

    }

    @Override
    public Permission getPermission() {
        return new Permission("module.fun.reddit");
    }

    @Override
    public String getLabel() {
        return "reddit";
    }

}
