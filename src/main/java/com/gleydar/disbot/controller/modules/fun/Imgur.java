package com.gleydar.disbot.controller.modules.fun;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.model.module.Module;
import com.gleydar.disbot.model.permissions.Permission;
import com.gleydar.disbot.utils.MessageUtil;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import net.dv8tion.jda.core.entities.Message;
import org.json.JSONObject;

/**
 * @author Gleydar
 */

public class Imgur extends Module {

	@Override
	public void execute(String[] args, Message m) throws Exception {
		int page = 0;

		if (args.length == 1) {
			try {
				page = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
				MessageUtil.sendChannelMessage(m.getChannel(), "Sorry, that is not a valid number");
			}
		}

		HttpResponse<JsonNode> r = Unirest.get("https://api.imgur.com/3/gallery/hot/viral/" + page)
				.header("Authorization", "Client-ID " + DisBot.IMGUR_ID).header("User-Agent", "discord:com.gleydar.disbot:0.1DEV").asJson();

		JSONObject n = r.getBody().getObject();
		int i = DisBot.r.nextInt(n.getJSONArray("data").length());
		JSONObject o = n.getJSONArray("data").getJSONObject(i);

		StringBuilder reply = new StringBuilder("Here is a random ");
		if (o.getBoolean("is_album")) {
			reply.append("album off the frontpage for you: ").append(o.getString("link"));

		} else if (o.getBoolean("animated")) {
			reply.append("gif off the frontpage for you: ").append(o.getString("link")).append(" with the tile: `").append(o.getString("title")).append("`");

		} else {
			reply.append("image off the frontpage for you: ").append(o.getString("link")).append(" with the tile: `").append(o.getString("title")).append("`");
		}
		MessageUtil.sendChannelMessage(m.getChannel(), reply.toString());
	}

	@Override
	public Permission getPermission() {
		return new Permission("module.fun.imgur");
	}

	@Override
	public String getLabel() {
		return "imgur";
	}

}
