package com.gleydar.disbot.controller.modules.help;

import com.gleydar.disbot.model.module.Module;
import com.gleydar.disbot.model.permissions.Permission;
import com.gleydar.disbot.utils.MessageUtil;
import net.dv8tion.jda.core.JDAInfo;
import net.dv8tion.jda.core.entities.Message;

/**
 * @author Gleydar
 */

public class Help extends Module {

	@Override
	public String getLabel() {
		return "help";
	}

	@Override
	public void execute(String[] args, Message m) {
		MessageUtil.sendChannelMessage(m.getChannel(), "Hi! I am DisBot :smile: \n " +
				"I was made by Gleydar and run on JDA version " + JDAInfo.VERSION + "\n" +
				" To see the commands you can use, please use `commands`");
	}

	@Override
	public Permission getPermission() {
		return new Permission("module.general.help");
	}


}
