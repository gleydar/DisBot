package com.gleydar.disbot.io.repositories;

import com.gleydar.disbot.model.user.DisUser;

/**
 * @author Konstantin on 28.06.2018.
 */
public class UserRepository extends CrudRepository<String, DisUser> {
}
