package com.gleydar.disbot.io.sockets;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.io.sockets.handlers.PacketHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.apache.commons.logging.impl.SimpleLog;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;

/**
 * Class for faking a Minecraft Server
 * Created by Konstantin on 13.03.2017.
 */
public class MinecraftSocket {

	public static byte[] cert;
	private static PrivateKey priv;
	private static String serverId;
	private static byte[] verifyToken = new byte[4];

	//Protocol Version > 47
	public static void run() throws Exception {
		setUpCertificate();
		setUpId();
		SimpleLog l = new SimpleLog("Minecraft");
		l.info("Socket opened.");
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
			ServerBootstrap b = new ServerBootstrap();
			b.group(bossGroup, workerGroup)
					.channel(NioServerSocketChannel.class)
					.childHandler(new ChannelInitializer<Channel>() {
						@Override
						public void initChannel(Channel ch) throws Exception {
							ch.pipeline().addLast(new PacketHandler());
						}
					})
					.option(ChannelOption.SO_BACKLOG, 128)
					.childOption(ChannelOption.SO_KEEPALIVE, true);
			ChannelFuture f = b.bind(25566).sync();
			f.channel().closeFuture().sync();
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();
		}
	}

	public static String getServerId() {
		return serverId;
	}

	public static byte[] getVerifyToken() {
		return verifyToken;
	}

	public static PrivateKey getPriv() {
		return priv;
	}

	private static void setUpCertificate() {
		KeyPairGenerator keygen = null;
		try {
			keygen = KeyPairGenerator.getInstance("RSA");
			keygen.initialize(1024);
			KeyPair pair = keygen.generateKeyPair();
			cert = pair.getPublic().getEncoded();
			priv = pair.getPrivate();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	private static void setUpId() {
		serverId = Long.toString(DisBot.r.nextLong(), 16);
		DisBot.r.nextBytes(verifyToken);
	}
}
