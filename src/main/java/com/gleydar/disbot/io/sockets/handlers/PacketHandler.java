package com.gleydar.disbot.io.sockets.handlers;

import com.gleydar.disbot.io.sockets.MinecraftSocket;
import com.gleydar.disbot.io.sockets.streams.MojangInputStream;
import com.gleydar.disbot.io.sockets.streams.MojangOutputStream;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.apache.commons.codec.Charsets;
import org.apache.commons.logging.impl.SimpleLog;

import javax.crypto.Cipher;
import java.io.EOFException;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.stream.Stream;

/**
 * Handles all Packets for the Minecraft Socket
 * Created by Konstantin on 14.03.2017.
 */
public class PacketHandler extends ChannelInboundHandlerAdapter {
	private boolean doLogin = false;
	private String username;
	private SimpleLog l = new SimpleLog("MinecraftPacket");

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		MojangInputStream in = new MojangInputStream((ByteBuf) msg);

		try {
			while (true) {
				// Initial Packet data
				int length = in.readInt();
				int id = in.readInt();
				MojangOutputStream out = new MojangOutputStream(Unpooled.buffer());
				MojangOutputStream data = new MojangOutputStream(Unpooled.buffer());

				// Packet IDs 0x00
				if (id == 0) {
					// Switch whether a Handshake or Login start is sent
					if (!doLogin) {
						// Handshake
						try {
							int version = in.readInt();
							String address = in.readUTF();
							int port = in.readUnsignedShort();
							int state = in.readInt();
							l.info("Received handshake: " +
									length + ", " + id + "\n Version: " + version +
									"\n from " + address + ", " + port + "\n with state " + state + "... ");
							in.close();
							if (state == 2) {
								// Switch to Login State
								doLogin = true;
								l.info("Client requested login, switching to next state.");
							} else {
								// Do a status response
								data.writeInt(0);
								//TODO add status response
							}
						} catch (Exception ignored) {

						}
					} else {
						// Login Start
						username = in.readUTF();
						l.info("Login requested for user " + username);
						in.close();

						// Encryption Request
						data.writeInt(1);
						data.writeUTF(MinecraftSocket.getServerId());
						data.writeInt(MinecraftSocket.cert.length);
						data.write(MinecraftSocket.cert);
						data.writeInt(MinecraftSocket.getVerifyToken().length);
						data.write(MinecraftSocket.getVerifyToken());
						data.close();
						l.debug("EncryptionRequest sent for user " + username);
					}

					// Packet ID 0x01
				} else if (id == 1) {
					if (doLogin) {
						l.debug("Got Encryption Response from " + username);

						int secretLength = in.readInt();

						byte[] secret = new byte[secretLength];
						for (int i = 0; i < secretLength; i++) {
							secret[i] = in.readByte();
						}

						int verifyLength = in.readInt();
						byte[] verify = new byte[verifyLength];
						for (int i = 0; i < verifyLength; i++) {
							verify[i] = in.readByte();
						}

						Cipher decrypt = Cipher.getInstance("RSA");
						decrypt.init(Cipher.DECRYPT_MODE, MinecraftSocket.getPriv());

						if (Arrays.equals(decrypt.doFinal(verify), MinecraftSocket.getVerifyToken())) {
							l.debug("Verify Token fits");

							// Decrypt the shared secret - We need it!
							byte[] aes = decrypt.doFinal(secret);

							byte[] hashId = getServerIdHash(MinecraftSocket.getServerId(), MinecraftSocket.cert, aes);
							String hash = (new BigInteger(hashId)).toString(16);
							l.debug("Hash: " + hash);

							HttpResponse<JsonNode> r =
									Unirest.get("https://sessionserver.mojang.com/session/minecraft/hasJoined?username="
											+ username + "&serverId=" + hash + "&ip=" + ((InetSocketAddress) ctx.channel().remoteAddress()).getAddress().getHostAddress()).asJson();
							try {
								l.info("Got a positive response (" + r.getBody().getObject().get("id") +
										") with the name " + r.getBody().getObject().get("name"));
							} catch (Exception e) {
								l.info("Didn't get a positive response." + r.getStatus());
							}

							data.writeInt(0);
							//TODO add kick respose
							return;
						} else {
							l.warn("Error while decrypting shared secret: " + Arrays.toString(secret));
						}

						doLogin = false;
					} else {
						long time = in.readLong();
						l.debug("Received ping packet: " + length + ", " + id + ", " + time);

						// Ping response
						data.writeInt(1);
						data.writeLong(time);
						data.close();
					}
				} else {
					System.out.println(id);
				}
				out.writeInt(data.writtenBytes());
				out.write(data.getData());
				out.close();
				ctx.writeAndFlush(out.buffer());
			}
		} catch (EOFException e) {

		}

	}

	private static byte[] getServerIdHash(String serverId, byte[] publicKey, byte[] secretKey) {
		return digestOperation("SHA-1"
				, serverId.getBytes(Charsets.ISO_8859_1), secretKey, publicKey);
	}

	private static byte[] digestOperation(String algo, byte[]... content) {
		try {
			MessageDigest messagedigest = MessageDigest.getInstance(algo);
			Stream.of(content).forEach(messagedigest::update);

			return messagedigest.digest();
		} catch (NoSuchAlgorithmException nosuchalgorithmexception) {
			nosuchalgorithmexception.printStackTrace();
			return null;
		}
	}
}