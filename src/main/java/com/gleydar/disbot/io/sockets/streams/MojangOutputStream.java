package com.gleydar.disbot.io.sockets.streams;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Konstantin on 14.03.2017.
 */
public class MojangOutputStream extends ByteBufOutputStream {

	public MojangOutputStream(ByteBuf buffer) {
		super(buffer);
	}

	@Override
	public void writeUTF(String s) throws IOException {
		byte[] data = s.getBytes();
		writeInt(s.length());
		write(data);
	}

	@Override
	public void writeInt(int paramInt) throws IOException {
		while (true) {
			if ((paramInt & 0xFFFFFF80) == 0) {
				writeByte(paramInt);
				return;
			}
			writeByte(paramInt & 0x7F | 0x80);
			paramInt >>>= 7;
		}
	}

	public byte[] getData() {
		return Arrays.copyOfRange(buffer().array(), 0, writtenBytes());
	}
}