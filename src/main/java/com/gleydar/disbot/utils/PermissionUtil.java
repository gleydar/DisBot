package com.gleydar.disbot.utils;

import com.gleydar.disbot.model.user.DisUser;
import com.gleydar.disbot.model.permissions.Permission;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Util class for checking permissions and the like.
 * Created by Gleydar on 22.02.2017.
 */
public class PermissionUtil {

	private static HashMap<DisUser, ArrayList<Permission>> globalPermissions = new HashMap<>();

	/**
	 * Checks if the user has the permission to use the specific command. First checks the individual users data.
	 * If the permission is denied or granted on this levels, a users group wont overwrite it. Otherwise his group is likewise
	 * checked. If a user doesnt have a group entry, a standard group will be used as fallback.
	 *
	 * @param user The DisUser instance of the Account to check
	 * @param perm The Permission to check
	 * @return true if the user has a permission, false otherwise.
	 */
	public static boolean hasGlobalPermission(DisUser user, Permission perm) {
		if (!globalPermissions.containsKey(user)) {
			// TODO
		}

		for (Permission p : globalPermissions.get(user)) {
			if (p.contains(perm)) {
				return p.isPositive();
			}
		}
		return false;
	}
}
