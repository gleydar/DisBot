package com.gleydar.disbot.utils;

import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class bundles all operations for the key-value yml storage.
 *
 * @author Gleydar
 */

public class ConfigUtil {

	private static YamlReader reader = null;
	private static YamlWriter writer = null;
	private static Map config = null;

	public static String getStringValue(String k) {
		if (config == null) {
			readFile();
		}
		return (String) config.get(k);
	}

	public static Object getObjectValue(String k) {
		if (config == null) {
			readFile();
		}
		return config.get(k);
	}

	public static Integer getIntValue(String k) {
		if (config == null) {
			readFile();
		}
		try {
			return Integer.parseInt((String) config.get(k));
		} catch (Exception e) {
			return 0;
		}
	}

	public void addValue(Object k, Object v) {
		config.put(k, v);
		writeFile();
	}

	public void addValue(String master, Object k, Object v) {
		if (config.get(master) instanceof Map) {
			Map m = (Map) config.get(master);
			m.put(k, v);
			writeFile();
		}
	}

	public static void forceUpdate() {
		readFile();
	}

	public static void readFile() {
		try {
			if (reader == null) {
				reader = new YamlReader(new FileReader("data.yml"));
			}
			config = (Map) reader.read();
		} catch (Exception ex) {
			Logger.getLogger(ConfigUtil.class.getName()).log(Level.SEVERE, "Couldn't read config file", ex);
		}
	}

	private static void writeFile() {
		try {
			writer = new YamlWriter(new FileWriter("data.yml"));
			writer.write(config);
		} catch (Exception ex) {
			Logger.getLogger(ConfigUtil.class.getName()).log(Level.SEVERE, "Couldn't write to config file", ex);
		}
	}

}
