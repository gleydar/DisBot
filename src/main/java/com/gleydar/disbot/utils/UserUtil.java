package com.gleydar.disbot.utils;

import com.gleydar.disbot.model.user.DisUser;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.User;

import java.util.HashMap;

/**
 * Class to store and retrieve DisUser objects
 * Created by kosch on 17.03.2017.
 */
public class UserUtil {

	private static HashMap<Guild, HashMap<String, DisUser>> userCache = new HashMap<>();

	public static DisUser getDisUser(User user, Guild g) {
		if (userCache.containsKey(g)) {
			HashMap<String, DisUser> users = userCache.get(g);
			if (users.containsKey(user.getId())) {
				return users.get(user.getId());
			} else {
				return fetchDisUser(user, g);
			}
		} else {
			return fetchDisUser(user, g);
		}
	}

	private static DisUser fetchDisUser(User user, Guild g) {
		//TODO try to fetch from DB
		return null;
	}
}
