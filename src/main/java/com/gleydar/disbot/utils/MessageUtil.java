package com.gleydar.disbot.utils;

import com.gleydar.disbot.model.user.DisUser;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.MessageEmbed;

/**
 * Created by Gleydar.
 */
public class MessageUtil {

	public static void sendPrivateMessage(DisUser c, String msg) {
		c.openPrivateChannel().complete().sendMessage(msg).queue();
	}

	public static void sendChannelMessage(MessageChannel c, String msg) {
		c.sendMessage(msg).queue();
	}

	public static void sendChannelMessage(MessageChannel c, Message msg) {
		c.sendMessage(msg).queue();
	}

	public static void sendChannelMessage(MessageChannel c, MessageEmbed msg) {
		c.sendMessage(msg).queue();
	}

	public static void sendPrivateMessage(DisUser c, Message msg) {
		c.openPrivateChannel().complete().sendMessage(msg).queue();
	}

	public static void sendPrivateMessage(DisUser c, MessageEmbed msg) {
		c.openPrivateChannel().complete().sendMessage(msg).queue();
	}
}
