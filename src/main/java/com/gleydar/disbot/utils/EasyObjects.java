package com.gleydar.disbot.utils;

import com.gleydar.disbot.DisBot;
import com.gleydar.disbot.model.user.DisUser;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.PrivateChannel;
import net.dv8tion.jda.core.entities.TextChannel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * @author Gleydar
 */

public class EasyObjects {

	private static HashMap<DisUser, PrivateChannel> channels = new HashMap<>();

	public static List<TextChannel> getChannelByName(String n) {
		JDA c = DisBot.getClient();
		List<TextChannel> list = new ArrayList<>();
		try {
			c.getTextChannelById(n).sendMessage("");
			list.add(c.getTextChannelById(n));
			return list;
		} catch (Exception e) {
			return c.getTextChannelsByName(n, true);
		}
	}

}
